package com.sportradar.scoreboard;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class ScoreboardApplicationTests {

	@Test
	void test_startGame_shouldCapture() {
			String homeTeamName = "Mexico";
			String awayTeamName = "Canada";
      Game game = new Game(new Team(homeTeamName), new Team(awayTeamName));
			ScoreBoard scoreBoard = new ScoreBoard();

			scoreBoard.startGame(game);
			assertThat(game.getScore()).isEqualTo(String.format("%s 0 - %s 0", homeTeamName, awayTeamName));
	}

	@Test
	void test_finishGame_shouldRemoveMatchFromScoreBoard() {
      String homeTeamName = "Mexico";
      String awayTeamName = "Canada";
      Game game = new Game(new Team(homeTeamName), new Team(awayTeamName));
			ScoreBoard scoreBoard = new ScoreBoard();

			scoreBoard.startGame(game);
			assertThat(scoreBoard.getGames()).hasSize(1);

			scoreBoard.stopGame(game);
			assertThat(scoreBoard.getGames()).isEmpty();
  }

	@Test
	void test_updateScore_gameScoreShouldBeUpdated() {
			String homeTeamName = "Mexico";
			String awayTeamName = "Canada";
			Game game = new Game(new Team(homeTeamName), new Team(awayTeamName));
			ScoreBoard scoreBoard = new ScoreBoard();

			scoreBoard.startGame(game);
			game.updateScore(1,1);
			assertThat(game.getScore()).isEqualTo(String.format("%s 1 - %s 1", homeTeamName, awayTeamName));

			game.updateScore(1,2);
			assertThat(game.getScore()).isEqualTo(String.format("%s 1 - %s 2", homeTeamName, awayTeamName));
	}

	@Test
	void test_getSummary_shouldOrderedByTotalScoreAndRecentlyAdded() {
			Game gameA = new Game(new Team("Mexico"), new Team("Canada"));
			Game gameB = new Game(new Team("Spain"), new Team("Brazil"));
			Game gameC = new Game(new Team("Germany"), new Team("France"));
			Game gameD = new Game(new Team("Uruguay"), new Team("Italy"));
			Game gameE = new Game(new Team("Argentina"), new Team("Australia"));

			ScoreBoard scoreBoard = new ScoreBoard();
			scoreBoard.startGame(gameA);
      scoreBoard.startGame(gameB);
      scoreBoard.startGame(gameC);
      scoreBoard.startGame(gameD);
      scoreBoard.startGame(gameE);

      gameA.updateScore(0,5);
      gameB.updateScore(10,2);
      gameC.updateScore(2,2);
      gameD.updateScore(6,6);
      gameE.updateScore(3,1);

      assertThat(scoreBoard.getSummary()).isEqualTo(
          """
              1. Uruguay 6 - Italy 6
              2. Spain 10 - Brazil 2
              3. Mexico 0 - Canada 5
              4. Argentina 3 - Australia 1
              5. Germany 2 - France 2""");
	}

}
