package com.sportradar.scoreboard;

import lombok.Getter;

@Getter
public class Team {

    private final String name;
    private int score;

    public Team(String name) {
        this.name = name;
    }

    public void updateScore(int score) {
        this.score = score;
    }

}
