package com.sportradar.scoreboard;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public class ScoreBoard {

    private final List<Game> games = new ArrayList<>();

    public void startGame(Game game) {
        game.updateScore(0, 0);
        games.add(game);
    }

    public void stopGame(Game game) {
        games.removeIf(g ->
            g.getHomeTeam().getName().equals(game.getHomeTeam().getName()) &&
            g.getAwayTeam().getName().equals(game.getAwayTeam().getName()));
    }

    public String getSummary() {

        List<Game> sortedGames = games.stream()
            .sorted(Game::compareTo)
            .toList();

        return createSummaryStr(sortedGames);
    }

    private String createSummaryStr(List<Game> sortedGames) {
        StringBuilder summary = new StringBuilder();

        for (int i = 0; i < sortedGames.size(); ++i) {
            int gameIndex = i + 1;

            Game game = sortedGames.get(i);
            summary.append(String.format("%d. %s", gameIndex, game.getScore()));

            if (gameIndex < sortedGames.size()) {
                summary.append("\n");
            }
        }

        return summary.toString();
    }

}
