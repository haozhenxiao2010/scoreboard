package com.sportradar.scoreboard;

import java.time.Instant;
import lombok.Getter;

@Getter
public class Game implements Comparable<Game> {

    private final Team homeTeam;
    private final Team awayTeam;
    private final Instant creationTime;

    public Game(Team homeTeam, Team awayTeam) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.creationTime = Instant.now();
    }

    public String getScore() {
        return String.format("%s %d - %s %d",
            homeTeam.getName(),
            homeTeam.getScore(),
            awayTeam.getName(),
            awayTeam.getScore());
    }

    public void updateScore(int homeScore, int awayScore) {
        homeTeam.updateScore(homeScore);
        awayTeam.updateScore(awayScore);
    }

    public int getTotalScore() {
        return homeTeam.getScore() + awayTeam.getScore();
    }

    @Override
    public int compareTo(Game game) {
        if (this.getTotalScore() == game.getTotalScore()) {
            return game.getCreationTime().compareTo(this.getCreationTime());
        }
        return game.getTotalScore() - this.getTotalScore();
    }
}
